/*
 * Game.h
 *
 *  Created on: 9 ago. 2019
 *      Author: joe
 */

#ifndef INCLUDE_GAME_H_
#define INCLUDE_GAME_H_

#include "Window.h"

class Game {
	public:
		Game();
		~Game();

		void HandleInput();
		void Update();
		void Render();

		Window* GetWindow();

		sf::Time GetElapsed();
		void RestartClock();
	private:

		void MoveMushroom();

		Window m_window;
		sf::Clock m_clock;
		sf::Time m_elapsed;

		sf::Texture m_mushroomTexture;
		sf::Sprite m_mushroom;
		sf::Vector2i m_increment;
};
#endif /* INCLUDE_GAME_H_ */
