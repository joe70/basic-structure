/*
 * Window.h
 *
 *  Created on: 9 ago. 2019
 *      Author: joe
 */

#ifndef INCLUDE_WINDOW_H_
#define INCLUDE_WINDOW_H_

#include <string>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>

class Window {
	public:
		Window();
		Window(const std::string& l_title, const sf::Vector2u& l_size);
		~Window();

		void BeginDraw();
		void EndDraw();

		void Update();

		bool IsDone();
		bool IsFullscreen();
		sf::Vector2u GetWindowSize();

		// sf::RenderWindow* GetRenderWindow();

		void ToggleFullscreen();

		void Draw(sf::Drawable& l_drawable);
	private:
		void Setup(const std::string title, const sf::Vector2u& size);
		void Create();
		void Destroy();

		sf::RenderWindow m_window;
		sf::Vector2u m_windowSize;
		std::string m_windowTitle;
		bool m_isDone;
		bool m_isFullscreen;
};
#endif /* INCLUDE_WINDOW_H_ */
